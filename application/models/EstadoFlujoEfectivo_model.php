<?php
class EstadoFlujoEfectivo_model extends CI_Model {
	private $cuentas_a_cargar;
	private $cuentas_transaccion_no_ajuste_actual = array();
    private $cuentas_transaccion_ajuste_actual = array();
    private $cuentas_transaccion_no_ajuste_pasada = array();
	private $cuentas_transacciones_comparadas = array();
	
	private $saldo_inicial_periodo = 0.0;
	private $efectivo_recibido_de_clientes = 0.0;
	private $efectivo_pagado_a_proveedores = 0.0;
	private $efectivo_usado_otras_operaciones = 0.0;
	private $efectivo_neto_usado_act_inversion = 0.0;
	private $efectivo_neto_usado_act_financiamiento = 0.0;
	
	private $efectivo_neto_usado_act_operacion = 0.0;
	private $disminucion_del_efectivo = 0.0;
	private $saldo_final_periodo = 0.0;
	
	private $cuenta_saldo_inicial_periodo = null;
	private $cuenta_act_operacion_cliente = array();
	private $cuenta_act_operacion_proveedor = array();
	private $cuenta_act_operacion_otros = array();
	private $cuenta_act_inversion = array();
	private $cuenta_act_financiamiento = array();
	
	public function __construct()
    {
        $this->load->database();
        $this->load->model('Cuenta_model');
        $this->load->model('CuentaEncapsulada_model');
    }
	
	public function cargar_informacion($id_periodo_actual, $id_periodo_anterior)
	{
		//Paso 1: obtener todas las cuentas
        $this->cuentas_a_cargar = $this->Cuenta_model->obtener_cuentas();
		
		// Paso 2: crear 2 objetos, uno que mantendra en vista las transacciones sin ajuste, las otras con ajuste
        foreach ($this->cuentas_a_cargar as $cuenta)
        {
			$cuenta_transaccion_no_ajuste_actual = new CuentaEncapsulada_model();
			$cuenta_transaccion_ajuste_actual = new CuentaEncapsulada_model();
			$cuenta_transaccion_no_ajuste_pasada = new CuentaEncapsulada_model();
			
			$cuenta_transaccion_no_ajuste_actual->cargar_cuenta($cuenta['id_cuenta_interno']);
			$cuenta_transaccion_ajuste_actual->cargar_cuenta($cuenta['id_cuenta_interno']);
			$cuenta_transaccion_no_ajuste_pasada->cargar_cuenta($cuenta['id_cuenta_interno']);
			
			
			$cuenta_transaccion_no_ajuste_actual->cargar_cuenta_periodo_flujo_efectivo($cuenta['id_cuenta_interno'], $id_periodo_actual);
			$cuenta_transaccion_ajuste_actual->cargar_cuenta_periodo_flujo_efectivo($cuenta['id_cuenta_interno'], $id_periodo_actual);
			$cuenta_transaccion_no_ajuste_pasada->cargar_cuenta_periodo_flujo_efectivo($cuenta['id_cuenta_interno'], $id_periodo_anterior);

			
			$cuenta_transaccion_no_ajuste_actual->saldar_cuenta('todo');
			$cuenta_transaccion_ajuste_actual->saldar_cuenta('ajuste_flujo_efectivo');
			$cuenta_transaccion_no_ajuste_pasada->saldar_cuenta('todo');
			
            /*$transaccion_no_ajuste->saldar_cuenta('no_ajuste');
            $transaccion_ajuste->saldar_cuenta('ajuste');
            $transaccion_todo->saldar_cuenta('todo');*/
            
            array_push($this->cuentas_transaccion_no_ajuste_actual, $cuenta_transaccion_no_ajuste_actual);
            array_push($this->cuentas_transaccion_ajuste_actual, $cuenta_transaccion_ajuste_actual);
            array_push($this->cuentas_transaccion_no_ajuste_pasada, $cuenta_transaccion_no_ajuste_pasada);
			
			
			// Paso 2.1: realizar la resta del periodo actual, menos el periodo anterior
			$cuenta_conjunta_comparada = new CuentaEncapsulada_model();
			$cuenta_conjunta_comparada->preparar_comparacion();
			
			$saldo_ajustado_actual = $cuenta_transaccion_ajuste_actual->obtener_saldo();
			$saldo_no_ajuste_pasada = $cuenta_transaccion_no_ajuste_pasada->obtener_saldo();
			
			if ($saldo_ajustado_actual[0] > 0)
			{
				$cuenta_conjunta_comparada->agregar_debe($saldo_ajustado_actual[0], 'sumar');
			}
			else
			{
				$cuenta_conjunta_comparada->agregar_haber(-$saldo_ajustado_actual[1], 'sumar');
			}
			
			if ($saldo_no_ajuste_pasada[0] > 0)
			{
				$cuenta_conjunta_comparada->agregar_debe($saldo_no_ajuste_pasada[0], 'restar');
			}
			else
			{
				$cuenta_conjunta_comparada->agregar_haber(-$saldo_no_ajuste_pasada[1], 'restar');
			}
			
			$cuenta_conjunta_comparada->calcular_efecto_sobre_efectivo();
			//echo var_dump($cuenta_conjunta_comparada);
			array_push($this->cuentas_transacciones_comparadas, $cuenta_conjunta_comparada);
			
			// Paso 2.2: realizar calculos de flujo de efectivo
			$tipo_cuenta = $cuenta_transaccion_ajuste_actual->obtener_datos_cuenta()['tipo_cuenta_flujo'];
			$valor_en_flujo = $cuenta_conjunta_comparada->obtener_efecto_sobre_efectivo();
			
			
			// Guardando datos para los array
			$datos = $cuenta_transaccion_ajuste_actual->obtener_datos_cuenta();
			$nombre = $datos['id_clase_cuenta'] . '.' . $datos['id_grupo_cuenta'] . '.' . $datos['id_cuenta_mayor'] . '.' . $datos['id_cuenta'];
			$nombre = $nombre . ' ' . $datos['nombre_cuenta'] . ' ' . $datos['abreviatura_clase_cuenta'];
			$efecto_sobre_efectivo = $cuenta_conjunta_comparada->obtener_efecto_sobre_efectivo();
			if ($efecto_sobre_efectivo < 0)
			{
				$efecto_sobre_efectivo = "(" . abs($efecto_sobre_efectivo) . ")";
			}
			
			$cuenta = array(
				'nombre' => $nombre,
				'efecto_sobre_efectivo' => $efecto_sobre_efectivo
			);
			
			
			if($tipo_cuenta == 'SIP')
			{
				$cuenta = array(
					'nombre' => $nombre,
					'efecto_sobre_efectivo' => 0.0
				);
				
				if ($saldo_no_ajuste_pasada[0] > -1)
				{
					$this->saldo_inicial_periodo = $saldo_no_ajuste_pasada[0];
					$cuenta['efecto_sobre_efectivo'] = $saldo_no_ajuste_pasada[0];
				}
				else
				{
					$this->saldo_inicial_periodo = $saldo_no_ajuste_pasada[1];
					$cuenta['efecto_sobre_efectivo'] = $saldo_no_ajuste_pasada[1];
				}
				
				$this->cuenta_saldo_inicial_periodo = $cuenta;
			}
			else if ($tipo_cuenta == 'AOC')
			{
				$this->efectivo_recibido_de_clientes += $valor_en_flujo;
				array_push($this->cuenta_act_operacion_cliente, $cuenta);
			}
			else if ($tipo_cuenta == 'AOP')
			{
				$this->efectivo_pagado_a_proveedores += $valor_en_flujo;
				array_push($this->cuenta_act_operacion_proveedor, $cuenta);
			}
			else if ($tipo_cuenta == 'AOO')
			{
				$this->efectivo_usado_otras_operaciones += $valor_en_flujo;
				array_push($this->cuenta_act_operacion_otros, $cuenta);
			}
			else if ($tipo_cuenta == 'ADI')
			{
				$this->efectivo_neto_usado_act_inversion += $valor_en_flujo;
				array_push($this->cuenta_act_inversion, $cuenta);
			}
			else if ($tipo_cuenta == 'ADF')
			{
				$this->efectivo_neto_usado_act_financiamiento += $valor_en_flujo;
				array_push($this->cuenta_act_financiamiento, $cuenta);
			}
		}
		
		$this->efectivo_neto_usado_act_operacion = $this->efectivo_recibido_de_clientes + $this->efectivo_pagado_a_proveedores + $this->efectivo_usado_otras_operaciones;
		$this->disminucion_del_efectivo = $this->efectivo_neto_usado_act_operacion + $this->efectivo_neto_usado_act_inversion + $this->efectivo_neto_usado_act_financiamiento;
		$this->saldo_final_periodo = $this->saldo_inicial_periodo + $this->disminucion_del_efectivo;
		
		/*echo "Efectivo neto en actividades de operacion: " . $this->efectivo_neto_usado_act_operacion . "\n";
		echo "Efectivo neto en actividades de inversion: " . $this->efectivo_neto_usado_act_inversion . "\n";
		echo "Efectivo neto en actividades de financiamiento: " . $this->efectivo_neto_usado_act_financiamiento . "\n";
		echo "Disminucion de efectivo total: " . $this->disminucion_del_efectivo . "\n";
		echo "Saldo al final del periodo: " . $this->saldo_final_periodo . "\n";*/
	}
	
	public function obtener_calculos_desplegar()
	{
		$filas_a_desplegar = array();
		$len = count($this->cuentas_transaccion_no_ajuste_actual);
		
		for ($i = 0; $i < $len; $i++)
		{
			// Paso 0: armar los datos de la cuenta
			$datos = $this->cuentas_transaccion_no_ajuste_actual[$i]->obtener_datos_cuenta();
			$nombre = $datos['id_clase_cuenta'] . '.' . $datos['id_grupo_cuenta'] . '.' . $datos['id_cuenta_mayor'] . '.' . $datos['id_cuenta'];
			$nombre = $nombre . ' ' . $datos['nombre_cuenta'] . ' ' . $datos['abreviatura_clase_cuenta'];
			
			// Paso 1: Saldo del periodo actual, sin ajustes ni eliminaciones
			$cuenta_sin_ajustar = $this->cuentas_transaccion_no_ajuste_actual[$i];
			$mat = $cuenta_sin_ajustar->obtener_saldo();
			$saldo_sin_ajustar_actual = '-';
			
			if ($mat[0] != -1)
			{
				$saldo_sin_ajustar_actual = "" . $mat[0];
			}
			else if ($mat[1] != -1 && $mat[1] != 0)
			{
				$saldo_sin_ajustar_actual = "(" . $mat[1] . ")";
			}
			else
			{
				$saldo_sin_ajustar_actual = '-';
			}
			
			
			// Paso 2: Saldo del periodo actual, con ajustes y eliminaciones
			$cuenta_ajustada = $this->cuentas_transaccion_ajuste_actual[$i];
			$mat = $cuenta_ajustada->obtener_saldo();
			$saldo_ajustado_actual = '';
			
			if ($mat[0] != -1)
			{
					$saldo_ajustado_actual = "" . $mat[0];
			}
			else if ($mat[1] != -1 && $mat[1] != 0)
			{
				$saldo_ajustado_actual = "(" . $mat[1] . ")";
			}
			else
			{
				$saldo_ajustado_actual = '-';
			}	
			
			
			// Paso 3: Saldo del periodo pasado, sin ajustes y eliminaciones
			$cuenta_pasada = $this->cuentas_transaccion_no_ajuste_pasada[$i];
			$mat = $cuenta_pasada->obtener_saldo();
			$saldo_pasado = '';
			
			if ($mat[0] != -1)
			{
				$saldo_pasado = "" . $mat[0];
			}
			else if ($mat[1] != -1 && $mat[1] != 0)
			{
				$saldo_pasado = "(" . $mat[1] . ")";
			}
			else
			{
				$saldo_pasado = "-";
			}
			
			// Paso 4: Aumentos y disminuciones, a modo general
			$cuenta_conjunta_comparada = $this->cuentas_transacciones_comparadas[$i];
			$mat = $cuenta_conjunta_comparada->obtener_saldo();
			$aumento_disminucion = "-";
			
			if ($mat[0] == 0 && $mat[1] == 0)
			{
				$aumento_disminucion = "-";
			}
			else if ($mat[0] != 0)
			{
				if ($mat[0] < 0)
				{
					$aumento_disminucion = "(" . abs($mat[0]) . ")";
				}
				else
				{
					$aumento_disminucion = abs($mat[0]);
				}
			}
			else
			{
				if ($mat[1] < 0)
				{
					$aumento_disminucion = "(" . abs($mat[1]) . ")";
				}
				else
				{
					$aumento_disminucion = abs($mat[1]);
				}
			}
			
			// Paso 5: Obtener el efecto sobre el efectivo
			$efecto_sobre_efectivo = $cuenta_conjunta_comparada->obtener_efecto_sobre_efectivo();
			
			if ($efecto_sobre_efectivo < 0)
			{
				$efecto_sobre_efectivo = "(" . abs($efecto_sobre_efectivo) . ")";
			}
			else
			{
				$efecto_sobre_efectivo = "" . $efecto_sobre_efectivo;
			}
			
			// Paso 6: Nombrar la categoria de aumento/disminucion a la que pertenece la cuenta
			$tipo_cuenta = $cuenta_sin_ajustar->obtener_datos_cuenta()['tipo_cuenta_flujo'];
			$tipo_cuenta_full = "-";
			
			if($tipo_cuenta == 'SIP')
			{
				$tipo_cuenta_full = "Saldo inicial del periodo";
			}
			else if ($tipo_cuenta == 'AOC')
			{
				$tipo_cuenta_full = "Actividad de operacion (Clientes)";
			}
			else if ($tipo_cuenta == 'AOP')
			{
				$tipo_cuenta_full = "Actividad de operacion (Proveedores)";
			}
			else if ($tipo_cuenta == 'AOO')
			{
				$tipo_cuenta_full = "Actividad de operacion (Otros)";
			}
			else if ($tipo_cuenta == 'ADI')
			{
				$tipo_cuenta_full = "Actividad de inversion";
			}
			else if ($tipo_cuenta == 'ADF')
			{
				$tipo_cuenta_full = "Actividad de financiamiento";
			}
			
			$cuenta = array(
				'nombre' => $nombre,
				'saldo_sin_ajustar_actual' => $saldo_sin_ajustar_actual,
				'saldo_ajustado_actual' => $saldo_ajustado_actual,
				'saldo_pasado' => $saldo_pasado,
				'aumento_disminucion' => $aumento_disminucion,
				'efecto_sobre_efectivo' => $efecto_sobre_efectivo,
				'tipo_cuenta_full' => $tipo_cuenta_full
			);
			
			array_push($filas_a_desplegar, $cuenta);
		}
		
		return $filas_a_desplegar;
	}
	
	public function obtener_cuenta_act_operacion_cliente()
	{
		return $this->cuenta_act_operacion_cliente;
	}
	
	public function obtener_total_act_operacion_cliente()
	{
		return $this->efectivo_recibido_de_clientes;
	}
	
	public function obtener_cuenta_act_operacion_proveedor()
	{
		return $this->cuenta_act_operacion_proveedor;
	}
	
	public function obtener_total_act_operacion_proveedor()
	{
		$valor = $this->efectivo_pagado_a_proveedores;
		
		if ($valor < 0)
		{
			$valor = "(" . abs($valor) . ")";
		}
		
		return $valor;
	}
	
	public function obtener_cuenta_act_operacion_otros()
	{
		return $this->cuenta_act_operacion_otros;
	}
	
	public function obtener_total_act_operacion_otros()
	{
		$valor = $this->efectivo_usado_otras_operaciones;
		
		if ($valor < 0)
		{
			$valor = "(" . abs($valor) . ")";
		}
		
		return $valor;
	}
	
	public function obtener_total_neto_act_operacion()
	{
		$valor = $this->efectivo_neto_usado_act_operacion;
		
		if ($valor < 0)
		{
			$valor = "(" . abs($valor) . ")";
		}
		
		return $valor;
	}
	
	public function obtener_cuenta_act_inversion()
	{
		return $this->cuenta_act_inversion;
	}
	
	public function obtener_total_act_inversion()
	{
		$valor = $this->efectivo_neto_usado_act_inversion;
		
		if ($valor < 0)
		{
			$valor = "(" . abs($valor) . ")";
		}
		
		return $valor;
	}
	
	public function obtener_cuenta_act_financiamiento()
	{
		return $this->cuenta_act_financiamiento;
	}
	
	public function obtener_total_act_financiamiento()
	{
		$valor = $this->efectivo_neto_usado_act_financiamiento;
		
		if ($valor < 0)
		{
			$valor = "(" . abs($valor) . ")";
		}
		
		return $valor;
	}
	
	public function obtener_disminucion_neta_efectivo()
	{
		$valor = $this->disminucion_del_efectivo;
		
		if ($valor < 0)
		{
			$valor = "(" . abs($valor) . ")";
		}
		
		return $valor;
	}
	
	public function obtener_saldo_inicial_periodo()
	{
		return $this->saldo_inicial_periodo;
	}
	
	public function obtener_total_final_periodo()
	{
		return $this->saldo_final_periodo;
	}
}