-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 01-12-2017 a las 02:10:52
-- Versión del servidor: 5.6.13
-- Versión de PHP: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `contabilidad_colegio`
--
CREATE DATABASE IF NOT EXISTS `contabilidad_colegio` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `contabilidad_colegio`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_actividad`
--

CREATE TABLE IF NOT EXISTS `tbl_actividad` (
  `id_actividad` int(11) NOT NULL AUTO_INCREMENT,
  `id_centro_costo` int(11) NOT NULL,
  `nombre_actividad` varchar(200) DEFAULT NULL,
  `descripcion_actividad` varchar(500) DEFAULT NULL,
  `tipo_actividad` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_actividad`),
  KEY `id_centro_costo` (`id_centro_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;


--
-- Estructura de tabla para la tabla `tbl_centro_costo`
--

CREATE TABLE IF NOT EXISTS `tbl_centro_costo` (
  `id_centro_costo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_centro_costo` varchar(200) DEFAULT NULL,
  `descripcion_centro_costo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id_centro_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_clase_cuenta`
--

CREATE TABLE IF NOT EXISTS `tbl_clase_cuenta` (
  `id_clase_cuenta` int(11) NOT NULL,
  `clase_cuenta` varchar(50) DEFAULT NULL,
  `abreviatura_clase_cuenta` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_clase_cuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_clase_cuenta`
--

INSERT INTO `tbl_clase_cuenta` (`id_clase_cuenta`, `clase_cuenta`, `abreviatura_clase_cuenta`) VALUES
(1, 'Activo', '(A)'),
(2, 'Pasivo', '(P)'),
(3, 'Capital', '(K)'),
(4, 'Ingreso', '(R)'),
(5, 'Egreso', '(R)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_configuracion`
--

CREATE TABLE IF NOT EXISTS `tbl_configuracion` (
  `perc_utilidad_retenida` int(11) DEFAULT NULL,
  `longitud_periodo_contable` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_configuracion`
--

INSERT INTO `tbl_configuracion` (`perc_utilidad_retenida`, `longitud_periodo_contable`) VALUES
(60, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_consumidor_costo`
--

CREATE TABLE IF NOT EXISTS `tbl_consumidor_costo` (
  `id_consumidor_costo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_consumidor_costo` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_consumidor_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_criterio_reparto_primario`
--

CREATE TABLE IF NOT EXISTS `tbl_criterio_reparto_primario` (
  `id_criterio_reparto_primario` int(11) NOT NULL AUTO_INCREMENT,
  `id_cuenta_interno` int(11) NOT NULL,
  `nombre_criterio_reparto_primario` varchar(150) DEFAULT NULL,
  `desc_criterio_reparto_primario` varchar(500) DEFAULT NULL,
  `unidad_reparto` varchar(10) DEFAULT NULL,
  `total_unidades` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_criterio_reparto_primario`),
  KEY `id_cuenta_interno` (`id_cuenta_interno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_criterio_reparto_secundario`
--

CREATE TABLE IF NOT EXISTS `tbl_criterio_reparto_secundario` (
  `id_criterio_reparto_secundario` int(11) NOT NULL AUTO_INCREMENT,
  `id_centro_costo` int(11) NOT NULL,
  `nombre_criterio_reparto_secundario` varchar(150) DEFAULT NULL,
  `desc_criterio_reparto_secundario` varchar(500) DEFAULT NULL,
  `unidad_reparto` varchar(10) DEFAULT NULL,
  `total_unidades` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_criterio_reparto_secundario`),
  KEY `id_centro_costo` (`id_centro_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_criterio_reparto_terciario`
--

CREATE TABLE IF NOT EXISTS `tbl_criterio_reparto_terciario` (
  `id_criterio_reparto_terciario` int(11) NOT NULL AUTO_INCREMENT,
  `id_actividad_auxiliar` int(11) NOT NULL,
  `nombre_criterio_reparto_terciario` varchar(150) DEFAULT NULL,
  `desc_criterio_reparto_terciario` varchar(500) DEFAULT NULL,
  `unidad_reparto` varchar(10) DEFAULT NULL,
  `total_unidades` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_criterio_reparto_terciario`),
  KEY `id_actividad_auxiliar` (`id_actividad_auxiliar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tbl_criterio_reparto_terciario`
--


--
-- Estructura de tabla para la tabla `tbl_cuenta`
--

CREATE TABLE IF NOT EXISTS `tbl_cuenta` (
  `id_cuenta_interno` int(11) NOT NULL AUTO_INCREMENT,
  `id_clase_cuenta` int(11) DEFAULT NULL,
  `id_grupo_cuenta` int(11) DEFAULT NULL,
  `id_cuenta_mayor` int(11) DEFAULT NULL,
  `id_cuenta` int(11) DEFAULT NULL,
  `nombre_cuenta` varchar(150) DEFAULT NULL,
  `atributo_cuenta` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_cuenta_interno`),
  KEY `id_clase_cuenta` (`id_clase_cuenta`),
  KEY `id_grupo_cuenta` (`id_grupo_cuenta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empleado`
--

CREATE TABLE IF NOT EXISTS `tbl_empleado` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_empleado` varchar(200) DEFAULT NULL,
  `fecha_contratacion` datetime DEFAULT NULL,
  `salario_base_diario` decimal(10,2) DEFAULT NULL,
  `tipo_empleado` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_empleado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_grupo_cuenta`
--

CREATE TABLE IF NOT EXISTS `tbl_grupo_cuenta` (
  `id_grupo_cuenta` int(11) NOT NULL,
  `grupo_cuenta` varchar(50) DEFAULT NULL,
  `abreviatura_grupo_cuenta` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_grupo_cuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_grupo_cuenta`
--

INSERT INTO `tbl_grupo_cuenta` (`id_grupo_cuenta`, `grupo_cuenta`, `abreviatura_grupo_cuenta`) VALUES
(1, 'Circulante', '(C)'),
(2, 'No Circulante', '(NC)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_inductor_consumido`
--

CREATE TABLE IF NOT EXISTS `tbl_inductor_consumido` (
  `id_inductor_consumido` int(11) NOT NULL AUTO_INCREMENT,
  `id_consumidor_costo` int(11) NOT NULL,
  `id_inductor_costo` int(11) NOT NULL,
  `valor_inductor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_inductor_consumido`),
  KEY `id_consumidor_costo` (`id_consumidor_costo`),
  KEY `id_inductor_costo` (`id_inductor_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Estructura de tabla para la tabla `tbl_inductor_costo`
--

CREATE TABLE IF NOT EXISTS `tbl_inductor_costo` (
  `id_inductor_costo` int(11) NOT NULL AUTO_INCREMENT,
  `id_actividad` int(11) NOT NULL,
  `nombre_inductor_costo` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_inductor_costo`),
  KEY `id_actividad` (`id_actividad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;


--
-- Estructura de tabla para la tabla `tbl_pago_empleado`
--

CREATE TABLE IF NOT EXISTS `tbl_pago_empleado` (
  `id_pago_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `id_empleado` int(11) NOT NULL,
  `monto_pago` decimal(10,2) DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `id_transaccion_1` int(11) NOT NULL,
  `id_transaccion_2` int(11) NOT NULL,
  PRIMARY KEY (`id_pago_empleado`),
  KEY `id_transaccion_1` (`id_transaccion_1`),
  KEY `id_transaccion_2` (`id_transaccion_2`),
  KEY `id_empleado` (`id_empleado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


--
-- Estructura de tabla para la tabla `tbl_periodo_contable`
--

CREATE TABLE IF NOT EXISTS `tbl_periodo_contable` (
  `id_periodo_contable` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_final` datetime DEFAULT NULL,
  `meses_duracion` int(11) DEFAULT NULL,
  `perc_utilidad_retenida` int(11) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_periodo_contable`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_transaccion`
--

CREATE TABLE IF NOT EXISTS `tbl_transaccion` (
  `id_transaccion` int(11) NOT NULL AUTO_INCREMENT,
  `id_periodo_contable` int(11) DEFAULT NULL,
  `id_cuenta_interno` int(11) DEFAULT NULL,
  `monto` decimal(20,2) DEFAULT NULL,
  `esta_en_debe` varchar(2) DEFAULT NULL,
  `es_transaccion_ajuste` varchar(2) DEFAULT NULL,
  `fecha_realizacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_transaccion`),
  KEY `id_periodo_contable` (`id_periodo_contable`),
  KEY `id_cuenta_interno` (`id_cuenta_interno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;


--
-- Estructura de tabla para la tabla `tbl_unidad_reparto_primario`
--

CREATE TABLE IF NOT EXISTS `tbl_unidad_reparto_primario` (
  `id_unidad_reparto_primario` int(11) NOT NULL AUTO_INCREMENT,
  `id_criterio_reparto_primario` int(11) NOT NULL,
  `id_centro_costo` int(11) NOT NULL,
  `valor_unidad` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_unidad_reparto_primario`),
  KEY `id_criterio_reparto_primario` (`id_criterio_reparto_primario`),
  KEY `id_centro_costo` (`id_centro_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;


--
-- Estructura de tabla para la tabla `tbl_unidad_reparto_secundario`
--

CREATE TABLE IF NOT EXISTS `tbl_unidad_reparto_secundario` (
  `id_unidad_reparto_secundario` int(11) NOT NULL AUTO_INCREMENT,
  `id_criterio_reparto_secundario` int(11) NOT NULL,
  `id_actividad` int(11) NOT NULL,
  `valor_unidad` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_unidad_reparto_secundario`),
  KEY `id_criterio_reparto_secundario` (`id_criterio_reparto_secundario`),
  KEY `id_actividad` (`id_actividad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Estructura de tabla para la tabla `tbl_unidad_reparto_terciario`
--

CREATE TABLE IF NOT EXISTS `tbl_unidad_reparto_terciario` (
  `id_unidad_reparto_terciario` int(11) NOT NULL AUTO_INCREMENT,
  `id_criterio_reparto_terciario` int(11) NOT NULL,
  `id_actividad` int(11) NOT NULL,
  `valor_unidad` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_unidad_reparto_terciario`),
  KEY `id_criterio_reparto_terciario` (`id_criterio_reparto_terciario`),
  KEY `id_actividad` (`id_actividad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Estructura de tabla para la tabla `tbl_usuario`
--

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nombre_completo` varchar(200) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `email`, `password`, `nombre_completo`, `tipo`) VALUES
(1, 'colegio@gmail.com', '2301c59cbdef6862ec9cb5935165d0de', 'Colegio', 'admin');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_actividad`
--
ALTER TABLE `tbl_actividad`
  ADD CONSTRAINT `tbl_actividad_ibfk_1` FOREIGN KEY (`id_centro_costo`) REFERENCES `tbl_centro_costo` (`id_centro_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_criterio_reparto_primario`
--
ALTER TABLE `tbl_criterio_reparto_primario`
  ADD CONSTRAINT `tbl_criterio_reparto_primario_ibfk_1` FOREIGN KEY (`id_cuenta_interno`) REFERENCES `tbl_cuenta` (`id_cuenta_interno`);

--
-- Filtros para la tabla `tbl_criterio_reparto_secundario`
--
ALTER TABLE `tbl_criterio_reparto_secundario`
  ADD CONSTRAINT `tbl_criterio_reparto_secundario_ibfk_1` FOREIGN KEY (`id_centro_costo`) REFERENCES `tbl_centro_costo` (`id_centro_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_criterio_reparto_terciario`
--
ALTER TABLE `tbl_criterio_reparto_terciario`
  ADD CONSTRAINT `tbl_criterio_reparto_terciario_ibfk_1` FOREIGN KEY (`id_actividad_auxiliar`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_cuenta`
--
ALTER TABLE `tbl_cuenta`
  ADD CONSTRAINT `tbl_cuenta_ibfk_1` FOREIGN KEY (`id_clase_cuenta`) REFERENCES `tbl_clase_cuenta` (`id_clase_cuenta`),
  ADD CONSTRAINT `tbl_cuenta_ibfk_2` FOREIGN KEY (`id_grupo_cuenta`) REFERENCES `tbl_grupo_cuenta` (`id_grupo_cuenta`);

--
-- Filtros para la tabla `tbl_inductor_consumido`
--
ALTER TABLE `tbl_inductor_consumido`
  ADD CONSTRAINT `tbl_inductor_consumido_ibfk_1` FOREIGN KEY (`id_consumidor_costo`) REFERENCES `tbl_consumidor_costo` (`id_consumidor_costo`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_inductor_consumido_ibfk_2` FOREIGN KEY (`id_inductor_costo`) REFERENCES `tbl_inductor_costo` (`id_inductor_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_inductor_costo`
--
ALTER TABLE `tbl_inductor_costo`
  ADD CONSTRAINT `tbl_inductor_costo_ibfk_1` FOREIGN KEY (`id_actividad`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_pago_empleado`
--
ALTER TABLE `tbl_pago_empleado`
  ADD CONSTRAINT `tbl_pago_empleado_ibfk_1` FOREIGN KEY (`id_transaccion_1`) REFERENCES `tbl_transaccion` (`id_transaccion`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pago_empleado_ibfk_2` FOREIGN KEY (`id_transaccion_2`) REFERENCES `tbl_transaccion` (`id_transaccion`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pago_empleado_ibfk_3` FOREIGN KEY (`id_empleado`) REFERENCES `tbl_empleado` (`id_empleado`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_transaccion`
--
ALTER TABLE `tbl_transaccion`
  ADD CONSTRAINT `tbl_transaccion_ibfk_1` FOREIGN KEY (`id_periodo_contable`) REFERENCES `tbl_periodo_contable` (`id_periodo_contable`),
  ADD CONSTRAINT `tbl_transaccion_ibfk_2` FOREIGN KEY (`id_cuenta_interno`) REFERENCES `tbl_cuenta` (`id_cuenta_interno`);

--
-- Filtros para la tabla `tbl_unidad_reparto_primario`
--
ALTER TABLE `tbl_unidad_reparto_primario`
  ADD CONSTRAINT `tbl_unidad_reparto_primario_ibfk_1` FOREIGN KEY (`id_criterio_reparto_primario`) REFERENCES `tbl_criterio_reparto_primario` (`id_criterio_reparto_primario`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_unidad_reparto_primario_ibfk_2` FOREIGN KEY (`id_centro_costo`) REFERENCES `tbl_centro_costo` (`id_centro_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_unidad_reparto_secundario`
--
ALTER TABLE `tbl_unidad_reparto_secundario`
  ADD CONSTRAINT `tbl_unidad_reparto_secundario_ibfk_1` FOREIGN KEY (`id_criterio_reparto_secundario`) REFERENCES `tbl_criterio_reparto_secundario` (`id_criterio_reparto_secundario`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_unidad_reparto_secundario_ibfk_2` FOREIGN KEY (`id_actividad`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_unidad_reparto_terciario`
--
ALTER TABLE `tbl_unidad_reparto_terciario`
  ADD CONSTRAINT `tbl_unidad_reparto_terciario_ibfk_1` FOREIGN KEY (`id_criterio_reparto_terciario`) REFERENCES `tbl_criterio_reparto_terciario` (`id_criterio_reparto_terciario`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_unidad_reparto_terciario_ibfk_2` FOREIGN KEY (`id_actividad`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
