-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-12-2018 a las 12:48:58
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contabilidad_colegio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_actividad`
--

CREATE TABLE `tbl_actividad` (
  `id_actividad` int(11) NOT NULL,
  `id_centro_costo` int(11) NOT NULL,
  `nombre_actividad` varchar(200) DEFAULT NULL,
  `descripcion_actividad` varchar(500) DEFAULT NULL,
  `tipo_actividad` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_centro_costo`
--

CREATE TABLE `tbl_centro_costo` (
  `id_centro_costo` int(11) NOT NULL,
  `nombre_centro_costo` varchar(200) DEFAULT NULL,
  `descripcion_centro_costo` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_clase_cuenta`
--

CREATE TABLE `tbl_clase_cuenta` (
  `id_clase_cuenta` int(11) NOT NULL,
  `clase_cuenta` varchar(50) DEFAULT NULL,
  `abreviatura_clase_cuenta` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_clase_cuenta`
--

INSERT INTO `tbl_clase_cuenta` (`id_clase_cuenta`, `clase_cuenta`, `abreviatura_clase_cuenta`) VALUES
(1, 'Activo', '(A)'),
(2, 'Pasivo', '(P)'),
(3, 'Capital', '(K)'),
(4, 'Ingreso', '(R)'),
(5, 'Egreso', '(R)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_configuracion`
--

CREATE TABLE `tbl_configuracion` (
  `perc_utilidad_retenida` int(11) DEFAULT NULL,
  `longitud_periodo_contable` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_configuracion`
--

INSERT INTO `tbl_configuracion` (`perc_utilidad_retenida`, `longitud_periodo_contable`) VALUES
(60, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_consumidor_costo`
--

CREATE TABLE `tbl_consumidor_costo` (
  `id_consumidor_costo` int(11) NOT NULL,
  `nombre_consumidor_costo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_criterio_reparto_primario`
--

CREATE TABLE `tbl_criterio_reparto_primario` (
  `id_criterio_reparto_primario` int(11) NOT NULL,
  `id_cuenta_interno` int(11) NOT NULL,
  `nombre_criterio_reparto_primario` varchar(150) DEFAULT NULL,
  `desc_criterio_reparto_primario` varchar(500) DEFAULT NULL,
  `unidad_reparto` varchar(10) DEFAULT NULL,
  `total_unidades` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_criterio_reparto_secundario`
--

CREATE TABLE `tbl_criterio_reparto_secundario` (
  `id_criterio_reparto_secundario` int(11) NOT NULL,
  `id_centro_costo` int(11) NOT NULL,
  `nombre_criterio_reparto_secundario` varchar(150) DEFAULT NULL,
  `desc_criterio_reparto_secundario` varchar(500) DEFAULT NULL,
  `unidad_reparto` varchar(10) DEFAULT NULL,
  `total_unidades` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_criterio_reparto_terciario`
--

CREATE TABLE `tbl_criterio_reparto_terciario` (
  `id_criterio_reparto_terciario` int(11) NOT NULL,
  `id_actividad_auxiliar` int(11) NOT NULL,
  `nombre_criterio_reparto_terciario` varchar(150) DEFAULT NULL,
  `desc_criterio_reparto_terciario` varchar(500) DEFAULT NULL,
  `unidad_reparto` varchar(10) DEFAULT NULL,
  `total_unidades` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cuenta`
--

CREATE TABLE `tbl_cuenta` (
  `id_cuenta_interno` int(11) NOT NULL,
  `id_clase_cuenta` int(11) DEFAULT NULL,
  `id_grupo_cuenta` int(11) DEFAULT NULL,
  `id_cuenta_mayor` int(11) DEFAULT NULL,
  `id_cuenta` int(11) DEFAULT NULL,
  `nombre_cuenta` varchar(150) DEFAULT NULL,
  `atributo_cuenta` varchar(200) DEFAULT NULL,
  `tipo_cuenta_flujo` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_cuenta`
--

INSERT INTO `tbl_cuenta` (`id_cuenta_interno`, `id_clase_cuenta`, `id_grupo_cuenta`, `id_cuenta_mayor`, `id_cuenta`, `nombre_cuenta`, `atributo_cuenta`, `tipo_cuenta_flujo`) VALUES
(19, 1, 1, 1, NULL, 'Efectivo y equivalentes', '', 'SIP'),
(20, 1, 1, 2, NULL, 'Clientes', '', 'AOC'),
(21, 1, 1, 3, NULL, 'Anticipos al personal', '', 'AOO'),
(23, 1, 1, 4, NULL, 'Pago a cuenta impuestos', '', 'AOO'),
(24, 1, 1, 5, NULL, 'Anticipos a proveedores', '', 'AOO'),
(25, 1, 1, 6, NULL, 'IVA por cobrar', '', 'AOO'),
(26, 1, 1, 7, NULL, 'Cuentas por cobrar entre compañias', '', 'AOO'),
(27, 1, 1, 8, NULL, 'Prestamos a socios', '', 'AOO'),
(28, 1, 1, 9, NULL, 'Deudores varios', '', 'AOO'),
(29, 1, 1, 10, NULL, 'Inventarios', '', 'AOP'),
(30, 1, 2, 11, NULL, 'Maquinaria y equipo', '', 'NP'),
(31, 1, 2, 12, NULL, 'Depreciacion de maquinaria y equipo', '', 'NP'),
(32, 1, 2, 13, NULL, 'Mobiliario y equipo', '', 'ADI'),
(33, 1, 2, 14, NULL, 'Depreciacion mobiliario y equipo', '', 'NP'),
(34, 1, 2, 15, NULL, 'Vehiculos', '', 'NP'),
(35, 1, 2, 16, NULL, 'Depreciacion vehiculos', '', 'NP'),
(36, 1, 2, 17, NULL, 'Herramientas', '', 'NP'),
(37, 1, 2, 18, NULL, 'Depreciacion de herramientas', '', 'NP'),
(38, 1, 2, 19, NULL, 'Revaluaciones de vehiculos', '', 'NP'),
(39, 1, 2, 20, NULL, 'Gastos anticipados', '', 'NP'),
(40, 1, 2, 21, NULL, 'Seguros anticipados', '', 'AOO'),
(41, 2, 1, 1, NULL, 'Prestamos', '', 'ADF'),
(42, 2, 1, 2, NULL, 'Proveedores del exterior', '', 'AOP'),
(43, 2, 1, 3, NULL, 'Proveedores locales', '', 'AOP'),
(44, 2, 1, 4, NULL, 'Retenciones', '', 'AOO'),
(45, 2, 1, 5, NULL, 'Impuestos por pagar', '', 'AOO'),
(46, 2, 1, 6, NULL, 'Anticipos de clientes', '', 'AOO'),
(47, 2, 1, 7, NULL, 'Depositos en garantia', '', 'AOO'),
(48, 2, 1, 8, NULL, 'IVA por pagar', '', 'AOO'),
(49, 3, 1, 1, NULL, 'Capital minimo', 'KE', 'NP'),
(50, 3, 1, 2, NULL, 'Capital variable', '', 'NP'),
(51, 3, 1, 3, NULL, 'Reserva legal', '', 'NP'),
(52, 3, 1, 4, NULL, 'Utilidad del ejercicio', '', 'ADF'),
(53, 3, 1, 5, NULL, 'Superavit por revaluacion', '', 'NP'),
(54, 5, 1, 1, NULL, 'Gastos por depreciacion de mobiliario y equipo', '', 'NP'),
(55, 4, 1, 1, NULL, 'Ventas netas', '', 'AOC'),
(56, 5, 1, 2, NULL, 'Costo de ventas', '', 'AOP'),
(57, 5, 1, 3, NULL, 'Gastos por seguro', '', 'AOO'),
(58, 5, 1, 4, NULL, 'Gastos por viajes y viaticos', '', 'AOO'),
(59, 5, 1, 5, NULL, 'Gastos de operacion', '', 'AOO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empleado`
--

CREATE TABLE `tbl_empleado` (
  `id_empleado` int(11) NOT NULL,
  `nombre_empleado` varchar(200) DEFAULT NULL,
  `fecha_contratacion` datetime DEFAULT NULL,
  `salario_base_diario` decimal(10,2) DEFAULT NULL,
  `tipo_empleado` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_grupo_cuenta`
--

CREATE TABLE `tbl_grupo_cuenta` (
  `id_grupo_cuenta` int(11) NOT NULL,
  `grupo_cuenta` varchar(50) DEFAULT NULL,
  `abreviatura_grupo_cuenta` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_grupo_cuenta`
--

INSERT INTO `tbl_grupo_cuenta` (`id_grupo_cuenta`, `grupo_cuenta`, `abreviatura_grupo_cuenta`) VALUES
(1, 'Circulante', '(C)'),
(2, 'No Circulante', '(NC)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_inductor_consumido`
--

CREATE TABLE `tbl_inductor_consumido` (
  `id_inductor_consumido` int(11) NOT NULL,
  `id_consumidor_costo` int(11) NOT NULL,
  `id_inductor_costo` int(11) NOT NULL,
  `valor_inductor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_inductor_costo`
--

CREATE TABLE `tbl_inductor_costo` (
  `id_inductor_costo` int(11) NOT NULL,
  `id_actividad` int(11) NOT NULL,
  `nombre_inductor_costo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_pago_empleado`
--

CREATE TABLE `tbl_pago_empleado` (
  `id_pago_empleado` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `monto_pago` decimal(10,2) DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `id_transaccion_1` int(11) NOT NULL,
  `id_transaccion_2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_periodo_contable`
--

CREATE TABLE `tbl_periodo_contable` (
  `id_periodo_contable` int(11) NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_final` datetime DEFAULT NULL,
  `meses_duracion` int(11) DEFAULT NULL,
  `perc_utilidad_retenida` int(11) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_periodo_contable`
--

INSERT INTO `tbl_periodo_contable` (`id_periodo_contable`, `fecha_creacion`, `fecha_inicio`, `fecha_final`, `meses_duracion`, `perc_utilidad_retenida`, `estado`) VALUES
(14, '2018-12-02 18:55:34', '2018-11-01 00:00:00', '2018-11-30 00:00:00', 1, 60, 'cerrado'),
(15, '2018-12-02 20:56:21', '2018-12-01 00:00:00', '2018-12-31 00:00:00', 1, 60, 'abierto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_transaccion`
--

CREATE TABLE `tbl_transaccion` (
  `id_transaccion` int(11) NOT NULL,
  `id_periodo_contable` int(11) DEFAULT NULL,
  `id_cuenta_interno` int(11) DEFAULT NULL,
  `monto` decimal(20,2) DEFAULT NULL,
  `esta_en_debe` varchar(2) DEFAULT NULL,
  `es_transaccion_ajuste` varchar(2) DEFAULT NULL,
  `fecha_realizacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_transaccion`
--

INSERT INTO `tbl_transaccion` (`id_transaccion`, `id_periodo_contable`, `id_cuenta_interno`, `monto`, `esta_en_debe`, `es_transaccion_ajuste`, `fecha_realizacion`) VALUES
(66, 14, 19, '463737.00', 'V', 'F', '2018-12-02 00:00:00'),
(67, 14, 20, '296685.00', 'V', 'F', '2018-12-02 00:00:00'),
(68, 14, 21, '14637.00', 'V', 'F', '2018-12-02 00:00:00'),
(69, 14, 23, '8429.00', 'V', 'F', '2018-12-02 00:00:00'),
(70, 14, 25, '18437.00', 'V', 'F', '2018-12-02 00:00:00'),
(71, 14, 26, '8418.00', 'V', 'F', '2018-12-02 00:00:00'),
(72, 14, 27, '21761.00', 'V', 'F', '2018-12-02 00:00:00'),
(73, 14, 28, '54576.00', 'V', 'F', '2018-12-02 00:00:00'),
(74, 14, 29, '411536.00', 'V', 'F', '2018-12-02 00:00:00'),
(75, 14, 30, '1531.00', 'V', 'F', '2018-12-02 00:00:00'),
(76, 14, 31, '459.00', 'F', 'F', '2018-12-02 00:00:00'),
(77, 14, 32, '94923.00', 'V', 'F', '2018-12-02 00:00:00'),
(78, 14, 33, '62987.00', 'F', 'F', '2018-12-02 00:00:00'),
(79, 14, 34, '112499.00', 'V', 'F', '2018-12-02 00:00:00'),
(80, 14, 35, '105462.00', 'F', 'F', '2018-12-02 00:00:00'),
(81, 14, 36, '1111.00', 'V', 'F', '2018-12-02 00:00:00'),
(82, 14, 37, '838.00', 'F', 'F', '2018-12-02 00:00:00'),
(83, 14, 38, '20200.00', 'V', 'F', '2018-12-02 00:00:00'),
(84, 14, 39, '33515.00', 'V', 'F', '2018-12-02 00:00:00'),
(85, 14, 40, '1247.00', 'V', 'F', '2018-12-02 00:00:00'),
(86, 14, 41, '455513.00', 'F', 'F', '2018-12-02 00:00:00'),
(87, 14, 42, '159763.00', 'F', 'F', '2018-12-02 00:00:00'),
(88, 14, 43, '103507.00', 'F', 'F', '2018-12-02 00:00:00'),
(89, 14, 44, '1924.00', 'F', 'F', '2018-12-02 00:00:00'),
(90, 14, 45, '5604.00', 'F', 'F', '2018-12-02 00:00:00'),
(91, 14, 46, '1998.00', 'F', 'F', '2018-12-02 00:00:00'),
(92, 14, 47, '248877.00', 'F', 'F', '2018-12-02 00:00:00'),
(93, 14, 48, '58.00', 'F', 'F', '2018-12-02 00:00:00'),
(94, 14, 49, '100000.00', 'F', 'F', '2018-12-02 00:00:00'),
(95, 14, 50, '140000.00', 'F', 'F', '2018-12-02 00:00:00'),
(96, 14, 51, '41357.00', 'F', 'F', '2018-12-02 00:00:00'),
(97, 14, 52, '114695.00', 'F', 'F', '2018-12-02 00:00:00'),
(98, 14, 53, '20200.00', 'F', 'F', '2018-12-02 00:00:00'),
(99, 15, 19, '60212.00', 'V', 'F', '2018-12-02 20:56:21'),
(100, 15, 20, '264023.00', 'V', 'F', '2018-12-02 20:56:21'),
(101, 15, 21, '5288.00', 'V', 'F', '2018-12-02 20:56:21'),
(102, 15, 23, '21423.00', 'V', 'F', '2018-12-02 20:56:21'),
(103, 15, 24, '272751.00', 'V', 'F', '2018-12-02 20:56:21'),
(104, 15, 25, '33099.00', 'V', 'F', '2018-12-02 20:56:21'),
(105, 15, 26, '8545.00', 'V', 'F', '2018-12-02 20:56:22'),
(106, 15, 27, '4136.00', 'V', 'F', '2018-12-02 20:56:22'),
(107, 15, 28, '59294.00', 'V', 'F', '2018-12-02 20:56:22'),
(108, 15, 29, '279242.00', 'V', 'F', '2018-12-02 20:56:22'),
(109, 15, 30, '1531.00', 'V', 'F', '2018-12-02 20:56:22'),
(110, 15, 31, '459.00', 'F', 'F', '2018-12-02 20:56:22'),
(111, 15, 32, '97841.00', 'V', 'F', '2018-12-02 20:56:22'),
(112, 15, 33, '63375.00', 'F', 'F', '2018-12-02 20:56:22'),
(113, 15, 34, '112499.00', 'V', 'F', '2018-12-02 20:56:22'),
(114, 15, 35, '109423.00', 'F', 'F', '2018-12-02 20:56:22'),
(115, 15, 36, '1111.00', 'V', 'F', '2018-12-02 20:56:22'),
(116, 15, 37, '838.00', 'F', 'F', '2018-12-02 20:56:22'),
(117, 15, 38, '15200.00', 'V', 'F', '2018-12-02 20:56:22'),
(118, 15, 39, '3515.00', 'V', 'F', '2018-12-02 20:56:22'),
(119, 15, 40, '2445.00', 'V', 'F', '2018-12-02 20:56:22'),
(120, 15, 41, '418315.00', 'F', 'F', '2018-12-02 20:56:22'),
(121, 15, 42, '41013.00', 'F', 'F', '2018-12-02 20:56:22'),
(122, 15, 43, '77981.00', 'F', 'F', '2018-12-02 20:56:22'),
(123, 15, 44, '1850.00', 'F', 'F', '2018-12-02 20:56:22'),
(124, 15, 45, '5212.00', 'F', 'F', '2018-12-02 20:56:22'),
(125, 15, 46, '64522.00', 'F', 'F', '2018-12-02 20:56:22'),
(126, 15, 47, '28858.00', 'F', 'F', '2018-12-02 20:56:22'),
(127, 15, 48, '0.00', 'F', 'F', '2018-12-02 20:56:22'),
(128, 15, 50, '180000.00', 'F', 'F', '2018-12-02 20:56:22'),
(129, 15, 51, '49928.00', 'F', 'F', '2018-12-02 20:56:23'),
(130, 15, 52, '85181.00', 'F', 'F', '2018-12-02 20:56:23'),
(131, 15, 53, '15200.00', 'F', 'F', '2018-12-02 20:56:23'),
(132, 15, 49, '100000.00', 'F', 'F', '2018-12-02 20:56:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_transaccion_flujo_efectivo`
--

CREATE TABLE `tbl_transaccion_flujo_efectivo` (
  `id_transaccion_flujo_efectivo` int(11) NOT NULL,
  `id_periodo_contable` int(11) NOT NULL,
  `id_cuenta_interno` int(11) NOT NULL,
  `monto` decimal(20,2) NOT NULL,
  `esta_en_debe` varchar(2) NOT NULL,
  `fecha_realizacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_transaccion_flujo_efectivo`
--

INSERT INTO `tbl_transaccion_flujo_efectivo` (`id_transaccion_flujo_efectivo`, `id_periodo_contable`, `id_cuenta_interno`, `monto`, `esta_en_debe`, `fecha_realizacion`) VALUES
(1, 15, 23, '28696.00', 'V', '2018-12-02 00:00:00'),
(2, 15, 38, '5000.00', 'V', '2018-12-02 00:00:00'),
(3, 15, 39, '30000.00', 'V', '2018-12-02 00:00:00'),
(4, 15, 40, '1247.00', 'V', '2018-12-02 00:00:00'),
(5, 15, 33, '388.00', 'V', '2018-12-02 00:00:00'),
(6, 15, 35, '3961.00', 'V', '2018-12-02 00:00:00'),
(7, 15, 50, '40000.00', 'V', '2018-12-02 00:00:00'),
(9, 15, 51, '8571.00', 'V', '2018-12-02 00:00:00'),
(10, 15, 53, '5000.00', 'F', '2018-12-02 00:00:00'),
(11, 15, 52, '85181.00', 'V', '2018-12-02 00:00:00'),
(12, 15, 52, '40000.00', 'F', '2018-12-02 00:00:00'),
(13, 15, 55, '3338246.00', 'F', '2018-12-02 00:00:00'),
(14, 15, 56, '2765261.00', 'V', '2018-12-02 00:00:00'),
(15, 15, 57, '2633.00', 'V', '2018-12-02 00:00:00'),
(16, 15, 58, '10169.00', 'V', '2018-12-02 00:00:00'),
(17, 15, 59, '402139.00', 'V', '2018-12-02 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_unidad_reparto_primario`
--

CREATE TABLE `tbl_unidad_reparto_primario` (
  `id_unidad_reparto_primario` int(11) NOT NULL,
  `id_criterio_reparto_primario` int(11) NOT NULL,
  `id_centro_costo` int(11) NOT NULL,
  `valor_unidad` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_unidad_reparto_secundario`
--

CREATE TABLE `tbl_unidad_reparto_secundario` (
  `id_unidad_reparto_secundario` int(11) NOT NULL,
  `id_criterio_reparto_secundario` int(11) NOT NULL,
  `id_actividad` int(11) NOT NULL,
  `valor_unidad` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_unidad_reparto_terciario`
--

CREATE TABLE `tbl_unidad_reparto_terciario` (
  `id_unidad_reparto_terciario` int(11) NOT NULL,
  `id_criterio_reparto_terciario` int(11) NOT NULL,
  `id_actividad` int(11) NOT NULL,
  `valor_unidad` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `id_usuario` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nombre_completo` varchar(200) NOT NULL,
  `tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `email`, `password`, `nombre_completo`, `tipo`) VALUES
(1, 'colegio@gmail.com', '2301c59cbdef6862ec9cb5935165d0de', 'Colegio', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_actividad`
--
ALTER TABLE `tbl_actividad`
  ADD PRIMARY KEY (`id_actividad`),
  ADD KEY `id_centro_costo` (`id_centro_costo`);

--
-- Indices de la tabla `tbl_centro_costo`
--
ALTER TABLE `tbl_centro_costo`
  ADD PRIMARY KEY (`id_centro_costo`);

--
-- Indices de la tabla `tbl_clase_cuenta`
--
ALTER TABLE `tbl_clase_cuenta`
  ADD PRIMARY KEY (`id_clase_cuenta`);

--
-- Indices de la tabla `tbl_consumidor_costo`
--
ALTER TABLE `tbl_consumidor_costo`
  ADD PRIMARY KEY (`id_consumidor_costo`);

--
-- Indices de la tabla `tbl_criterio_reparto_primario`
--
ALTER TABLE `tbl_criterio_reparto_primario`
  ADD PRIMARY KEY (`id_criterio_reparto_primario`),
  ADD KEY `id_cuenta_interno` (`id_cuenta_interno`);

--
-- Indices de la tabla `tbl_criterio_reparto_secundario`
--
ALTER TABLE `tbl_criterio_reparto_secundario`
  ADD PRIMARY KEY (`id_criterio_reparto_secundario`),
  ADD KEY `id_centro_costo` (`id_centro_costo`);

--
-- Indices de la tabla `tbl_criterio_reparto_terciario`
--
ALTER TABLE `tbl_criterio_reparto_terciario`
  ADD PRIMARY KEY (`id_criterio_reparto_terciario`),
  ADD KEY `id_actividad_auxiliar` (`id_actividad_auxiliar`);

--
-- Indices de la tabla `tbl_cuenta`
--
ALTER TABLE `tbl_cuenta`
  ADD PRIMARY KEY (`id_cuenta_interno`),
  ADD KEY `id_clase_cuenta` (`id_clase_cuenta`),
  ADD KEY `id_grupo_cuenta` (`id_grupo_cuenta`);

--
-- Indices de la tabla `tbl_empleado`
--
ALTER TABLE `tbl_empleado`
  ADD PRIMARY KEY (`id_empleado`);

--
-- Indices de la tabla `tbl_grupo_cuenta`
--
ALTER TABLE `tbl_grupo_cuenta`
  ADD PRIMARY KEY (`id_grupo_cuenta`);

--
-- Indices de la tabla `tbl_inductor_consumido`
--
ALTER TABLE `tbl_inductor_consumido`
  ADD PRIMARY KEY (`id_inductor_consumido`),
  ADD KEY `id_consumidor_costo` (`id_consumidor_costo`),
  ADD KEY `id_inductor_costo` (`id_inductor_costo`);

--
-- Indices de la tabla `tbl_inductor_costo`
--
ALTER TABLE `tbl_inductor_costo`
  ADD PRIMARY KEY (`id_inductor_costo`),
  ADD KEY `id_actividad` (`id_actividad`);

--
-- Indices de la tabla `tbl_pago_empleado`
--
ALTER TABLE `tbl_pago_empleado`
  ADD PRIMARY KEY (`id_pago_empleado`),
  ADD KEY `id_transaccion_1` (`id_transaccion_1`),
  ADD KEY `id_transaccion_2` (`id_transaccion_2`),
  ADD KEY `id_empleado` (`id_empleado`);

--
-- Indices de la tabla `tbl_periodo_contable`
--
ALTER TABLE `tbl_periodo_contable`
  ADD PRIMARY KEY (`id_periodo_contable`);

--
-- Indices de la tabla `tbl_transaccion`
--
ALTER TABLE `tbl_transaccion`
  ADD PRIMARY KEY (`id_transaccion`),
  ADD KEY `id_periodo_contable` (`id_periodo_contable`),
  ADD KEY `id_cuenta_interno` (`id_cuenta_interno`);

--
-- Indices de la tabla `tbl_transaccion_flujo_efectivo`
--
ALTER TABLE `tbl_transaccion_flujo_efectivo`
  ADD PRIMARY KEY (`id_transaccion_flujo_efectivo`),
  ADD KEY `id_periodo_contable` (`id_periodo_contable`),
  ADD KEY `id_cuenta_interno` (`id_cuenta_interno`);

--
-- Indices de la tabla `tbl_unidad_reparto_primario`
--
ALTER TABLE `tbl_unidad_reparto_primario`
  ADD PRIMARY KEY (`id_unidad_reparto_primario`),
  ADD KEY `id_criterio_reparto_primario` (`id_criterio_reparto_primario`),
  ADD KEY `id_centro_costo` (`id_centro_costo`);

--
-- Indices de la tabla `tbl_unidad_reparto_secundario`
--
ALTER TABLE `tbl_unidad_reparto_secundario`
  ADD PRIMARY KEY (`id_unidad_reparto_secundario`),
  ADD KEY `id_criterio_reparto_secundario` (`id_criterio_reparto_secundario`),
  ADD KEY `id_actividad` (`id_actividad`);

--
-- Indices de la tabla `tbl_unidad_reparto_terciario`
--
ALTER TABLE `tbl_unidad_reparto_terciario`
  ADD PRIMARY KEY (`id_unidad_reparto_terciario`),
  ADD KEY `id_criterio_reparto_terciario` (`id_criterio_reparto_terciario`),
  ADD KEY `id_actividad` (`id_actividad`);

--
-- Indices de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_actividad`
--
ALTER TABLE `tbl_actividad`
  MODIFY `id_actividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tbl_centro_costo`
--
ALTER TABLE `tbl_centro_costo`
  MODIFY `id_centro_costo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_consumidor_costo`
--
ALTER TABLE `tbl_consumidor_costo`
  MODIFY `id_consumidor_costo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_criterio_reparto_primario`
--
ALTER TABLE `tbl_criterio_reparto_primario`
  MODIFY `id_criterio_reparto_primario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_criterio_reparto_secundario`
--
ALTER TABLE `tbl_criterio_reparto_secundario`
  MODIFY `id_criterio_reparto_secundario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_criterio_reparto_terciario`
--
ALTER TABLE `tbl_criterio_reparto_terciario`
  MODIFY `id_criterio_reparto_terciario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_cuenta`
--
ALTER TABLE `tbl_cuenta`
  MODIFY `id_cuenta_interno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `tbl_empleado`
--
ALTER TABLE `tbl_empleado`
  MODIFY `id_empleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_inductor_consumido`
--
ALTER TABLE `tbl_inductor_consumido`
  MODIFY `id_inductor_consumido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tbl_inductor_costo`
--
ALTER TABLE `tbl_inductor_costo`
  MODIFY `id_inductor_costo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_pago_empleado`
--
ALTER TABLE `tbl_pago_empleado`
  MODIFY `id_pago_empleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_periodo_contable`
--
ALTER TABLE `tbl_periodo_contable`
  MODIFY `id_periodo_contable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tbl_transaccion`
--
ALTER TABLE `tbl_transaccion`
  MODIFY `id_transaccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT de la tabla `tbl_transaccion_flujo_efectivo`
--
ALTER TABLE `tbl_transaccion_flujo_efectivo`
  MODIFY `id_transaccion_flujo_efectivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `tbl_unidad_reparto_primario`
--
ALTER TABLE `tbl_unidad_reparto_primario`
  MODIFY `id_unidad_reparto_primario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tbl_unidad_reparto_secundario`
--
ALTER TABLE `tbl_unidad_reparto_secundario`
  MODIFY `id_unidad_reparto_secundario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tbl_unidad_reparto_terciario`
--
ALTER TABLE `tbl_unidad_reparto_terciario`
  MODIFY `id_unidad_reparto_terciario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_actividad`
--
ALTER TABLE `tbl_actividad`
  ADD CONSTRAINT `tbl_actividad_ibfk_1` FOREIGN KEY (`id_centro_costo`) REFERENCES `tbl_centro_costo` (`id_centro_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_criterio_reparto_primario`
--
ALTER TABLE `tbl_criterio_reparto_primario`
  ADD CONSTRAINT `tbl_criterio_reparto_primario_ibfk_1` FOREIGN KEY (`id_cuenta_interno`) REFERENCES `tbl_cuenta` (`id_cuenta_interno`);

--
-- Filtros para la tabla `tbl_criterio_reparto_secundario`
--
ALTER TABLE `tbl_criterio_reparto_secundario`
  ADD CONSTRAINT `tbl_criterio_reparto_secundario_ibfk_1` FOREIGN KEY (`id_centro_costo`) REFERENCES `tbl_centro_costo` (`id_centro_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_criterio_reparto_terciario`
--
ALTER TABLE `tbl_criterio_reparto_terciario`
  ADD CONSTRAINT `tbl_criterio_reparto_terciario_ibfk_1` FOREIGN KEY (`id_actividad_auxiliar`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_cuenta`
--
ALTER TABLE `tbl_cuenta`
  ADD CONSTRAINT `tbl_cuenta_ibfk_1` FOREIGN KEY (`id_clase_cuenta`) REFERENCES `tbl_clase_cuenta` (`id_clase_cuenta`),
  ADD CONSTRAINT `tbl_cuenta_ibfk_2` FOREIGN KEY (`id_grupo_cuenta`) REFERENCES `tbl_grupo_cuenta` (`id_grupo_cuenta`);

--
-- Filtros para la tabla `tbl_inductor_consumido`
--
ALTER TABLE `tbl_inductor_consumido`
  ADD CONSTRAINT `tbl_inductor_consumido_ibfk_1` FOREIGN KEY (`id_consumidor_costo`) REFERENCES `tbl_consumidor_costo` (`id_consumidor_costo`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_inductor_consumido_ibfk_2` FOREIGN KEY (`id_inductor_costo`) REFERENCES `tbl_inductor_costo` (`id_inductor_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_inductor_costo`
--
ALTER TABLE `tbl_inductor_costo`
  ADD CONSTRAINT `tbl_inductor_costo_ibfk_1` FOREIGN KEY (`id_actividad`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_pago_empleado`
--
ALTER TABLE `tbl_pago_empleado`
  ADD CONSTRAINT `tbl_pago_empleado_ibfk_1` FOREIGN KEY (`id_transaccion_1`) REFERENCES `tbl_transaccion` (`id_transaccion`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pago_empleado_ibfk_2` FOREIGN KEY (`id_transaccion_2`) REFERENCES `tbl_transaccion` (`id_transaccion`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pago_empleado_ibfk_3` FOREIGN KEY (`id_empleado`) REFERENCES `tbl_empleado` (`id_empleado`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_transaccion`
--
ALTER TABLE `tbl_transaccion`
  ADD CONSTRAINT `tbl_transaccion_ibfk_1` FOREIGN KEY (`id_periodo_contable`) REFERENCES `tbl_periodo_contable` (`id_periodo_contable`),
  ADD CONSTRAINT `tbl_transaccion_ibfk_2` FOREIGN KEY (`id_cuenta_interno`) REFERENCES `tbl_cuenta` (`id_cuenta_interno`);

--
-- Filtros para la tabla `tbl_transaccion_flujo_efectivo`
--
ALTER TABLE `tbl_transaccion_flujo_efectivo`
  ADD CONSTRAINT `tbl_transaccion_flujo_efectivo_ibfk_1` FOREIGN KEY (`id_periodo_contable`) REFERENCES `tbl_periodo_contable` (`id_periodo_contable`),
  ADD CONSTRAINT `tbl_transaccion_flujo_efectivo_ibfk_2` FOREIGN KEY (`id_cuenta_interno`) REFERENCES `tbl_cuenta` (`id_cuenta_interno`);

--
-- Filtros para la tabla `tbl_unidad_reparto_primario`
--
ALTER TABLE `tbl_unidad_reparto_primario`
  ADD CONSTRAINT `tbl_unidad_reparto_primario_ibfk_1` FOREIGN KEY (`id_criterio_reparto_primario`) REFERENCES `tbl_criterio_reparto_primario` (`id_criterio_reparto_primario`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_unidad_reparto_primario_ibfk_2` FOREIGN KEY (`id_centro_costo`) REFERENCES `tbl_centro_costo` (`id_centro_costo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_unidad_reparto_secundario`
--
ALTER TABLE `tbl_unidad_reparto_secundario`
  ADD CONSTRAINT `tbl_unidad_reparto_secundario_ibfk_1` FOREIGN KEY (`id_criterio_reparto_secundario`) REFERENCES `tbl_criterio_reparto_secundario` (`id_criterio_reparto_secundario`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_unidad_reparto_secundario_ibfk_2` FOREIGN KEY (`id_actividad`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbl_unidad_reparto_terciario`
--
ALTER TABLE `tbl_unidad_reparto_terciario`
  ADD CONSTRAINT `tbl_unidad_reparto_terciario_ibfk_1` FOREIGN KEY (`id_criterio_reparto_terciario`) REFERENCES `tbl_criterio_reparto_terciario` (`id_criterio_reparto_terciario`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_unidad_reparto_terciario_ibfk_2` FOREIGN KEY (`id_actividad`) REFERENCES `tbl_actividad` (`id_actividad`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
